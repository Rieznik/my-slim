<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/*
$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');
*/

$app->get('/', function (Request $request, Response $response, $args) {
    try {
        $search = $request->getQueryParam('search');
        if($search) {
            //hledame
            $stmt = $this->db->prepare('SELECT *
                                  FROM person
                                  WHERE 
                                    first_name ILIKE :s OR
                                    last_name ILIKE :s OR 
                                    nickname ILIKE :s
                                  ORDER BY last_name');
            $stmt->bindValue(':s', '%' . $search . '%');
            $stmt->execute();
        } else {
            //nehledame
            $stmt = $this->db->query('SELECT *
                                  FROM person
                                  ORDER BY last_name');
        }
        $tplVars['persons'] = $stmt->fetchAll();
        return $this->view->render($response, 'persons.latte', $tplVars);
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('index');

//zobrazit formular
$app->get('/create-person', function (Request $request, Response $response, $args) {
    return $this->view->render($response, 'create-persons.latte');
})->setName('createPerson');

//zpracovat formular
$app->post('/create-person', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if(!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])) {
        try {
            $stmt = $this->db->prepare('INSERT INTO person
                           (birth_day, height, gender, first_name, last_name, nickname)
                           VALUES (:bd, :h, :g, :fn, :ln, :nn)');

            $stmt->bindValue(':bd', empty($data['bd']) ? null : $data['bd']);
            $stmt->bindValue(':h', empty($data['h']) ? null : $data['h']);
            $stmt->bindValue(':g', empty($data['g']) ? null : $data['g']);

            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->execute();
            //presmerovat na vypis osob
            return $response->withHeader('Location',
                $this->router->pathFor('index') . '?search=' . $data['fn']);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            die($e->getMessage());
        }
    } else {
        die('Nebylo zadano vse potrebne');
    }
});
